package crocodile8000.sliding;

import java.util.LinkedList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;



/**
 * ����������� - �� ������������ (��������� ������� ����) (IT'S TESTING, DON'T USE)
 * SlidingDoubleLayout - Layout � ������������ ��������� ������ ���� (���� � ��������)
 * 
 * ���������� ������ layout, �������� ���������� � ������� TranslateAnimation, 
 * ��� ��������� onTouchEvent - ����������� ������� ������� .layout(...),
 * ��������� (��� ���) ������� �� �������� - � ������ onInterceptTouchEvent(...)
 * ������� ������ ������� - int maxX
 * @author Crocodile8008
 *
 */

public class SlidingDoubleLayout extends FrameLayout {
	
	public final static String LOG = "SlidingLay";
	
	public static final int STATE_MENU = -1;
	public static final int STATE_NORMAL = 0;
	private int state = STATE_NORMAL;
	
	private int toleranceToMove, maxX, dividerXleft, dividerXright;
	
	private int tX, tXtoMove, tXstart, tXinner, tYstart, xOld;
	private int layW, layH;
	
	private boolean isHorizontalMove, toleranceOver;
	private boolean dontSlideNow;
	
	private Animation movement, correcting;
	private Interpolator interpolator;

	private final static float CORR_SPEED = 0.002f;
	
	public List <FrameLayoutHelper> screen;
	public FrameLayout leftMenu;
	private int currSlideNumber;
	
	
	
	
	
	@SuppressLint("NewApi")
	public SlidingDoubleLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(); }

	public SlidingDoubleLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(); }

	public SlidingDoubleLayout(Context context) {
		super(context);
		init(); }

	private void init(){
		interpolator = new LinearInterpolator();
	}
	
	
	public void initBaseView(View mainView, View menuView){
		currSlideNumber = 0;
		
		leftMenu = new FrameLayout(getContext());
		leftMenu.addView(menuView);
		this.addView(leftMenu);
		
		screen = new LinkedList<FrameLayoutHelper>();
		screen.add( new FrameLayoutHelper(getContext()) );
		screen.get(currSlideNumber).addView(mainView);
		this.addView(screen.get(currSlideNumber));
		screen.get(currSlideNumber).setNumber(screen.size()-1);
		screen.get(currSlideNumber).setBackgroundColor(Color.LTGRAY);
		
		setIgnoredSlidingView(leftMenu);
//		initNotSlidingView((ViewGroup) mainView);
	}
	
	
	public void addOneMorePage(View newView){
		screen.add( new FrameLayoutHelper(getContext()) );
		screen.get(screen.size()-1).addView(newView);
		this.addView(screen.get(screen.size()-1));
		screen.get(screen.size()-1).setNumber(screen.size()-1);
		screen.get(screen.size()-1).layout(220, 0, 480 , 320);
		Log.d(LOG, "screen.size()  "+screen.size()+ "  layW "+layW);
//		initNotSlidingView((ViewGroup) newView);
	}
	
	
	public void initNotSlidingView(ViewGroup v){
		Log.w(LOG, "initNotSlidingView ");
		int childCnt = v.getChildCount();
		View child;
		for (int i = 0; i < childCnt; i++) {
			child = v.getChildAt(i);
			if(child instanceof HorizontalScrollView){
				Log.w(LOG, "instanceof HorizontalScrollView "+child);
				this.setIgnoredSlidingView(child);
			}
		}
	}
	
	
	/**
	 * ������ ���������
	 */
	public void setDontSlide(){
		dontSlideNow = true; }
	
	/**
	 * ���������� ���������
	 */
	public void setCanSlide(){
		dontSlideNow = false; }
	
	/**
	 * ����� ����� �������� ������
	 */
	public int getCurrentLayoutNumber(){
		return currSlideNumber;
	}
	
	/**
	 * ����� ������� ���������
	 */
	public int getCurrentState(){
		return state;
	}
	
	
	
	
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		super.onLayout( changed,  l,  t,  r,  b);
		
		if (layW <1) layW = getWidth();
		if (layH <1) layH  = getHeight();
		
		toleranceToMove = layW/20;	// ����� ���������������� ��� ����������� ������������� ����������� �������
		maxX = layW - layW/3; 		// ������������ �������� ������ ������
		dividerXleft = maxX/4; 	// ���������� ������� �� ������� ������ ������ � ������ ���������
		dividerXright = maxX/4; 	// ���������� ������� �� ������� ������ ������ � ������ ���������
		
		setState();

		Log.w(LOG, "MAIN onLayout/ dividerXleft "+dividerXleft + "   dividerXright "+dividerXright);
		
		if(screen.size()>1)
			for(int i = 1; i <screen.size(); i++)
				screen.get(i).layout(layW, 0, layW*2 , layH);
	}

	
	private void setState(){
		if (screen.get(currSlideNumber).x > layW/2) {
			state = STATE_MENU;
		}
		else {
			state = STATE_NORMAL;
		}
		Log.v(LOG, "state "+state );
	}
	
	

	
	
	
	
	//TODO
	/**
	 * ��������� ������� �������� ����
	 */
	@Override
	public boolean onInterceptTouchEvent(MotionEvent event) {
		
		if (dontSlideNow) return false;

	    if (event.getAction() == MotionEvent.ACTION_DOWN) {
			toleranceOver = false;
			isHorizontalMove = false;
	    	tYstart = (int)event.getY();
	    	tXstart = (int)event.getX();
	    } 
	    
		if (state == STATE_MENU && tXstart < maxX) {
			Log.d(LOG, ""+STATE_MENU);
			return false;
		}
		
	    else if (event.getAction() == MotionEvent.ACTION_MOVE) {
	    	// ����������, �� ����� ��� �������� ������ � ���� �� � �� ������� layouyt, 
	    	// ����� ��������� ��������� ������� ������� (boolean isHorizontalMove)
	    	if (!toleranceOver){
		        if (
		        Math.abs(tYstart - event.getY()) > toleranceToMove  || Math.abs(tXstart - event.getX()) > toleranceToMove) {
		        	toleranceOver = true;
		        	Log.i(LOG, "ToleranceOver! ");
		        }
		    }
	    	else{
		        if ( Math.abs(tYstart - event.getY()) >  Math.abs(tXstart - event.getX()) ){
		        	isHorizontalMove = false; }
		        else{
		        	isHorizontalMove = true; 
					tXinner = (int)event.getX();
					xOld = (int)event.getX()-tXinner;	
		        }
	        	Log.i(LOG, "isHorizontalMove "+isHorizontalMove);
	        }
	    } 
	    else if (event.getAction() == MotionEvent.ACTION_UP) {
	    	isHorizontalMove = false;
	    	toleranceOver = false;
	    }
	    return isHorizontalMove;
	}

	
	
	
	
	
	
	
	
	//TODO
	/**
	 * ������� ��������� ���� �������
	 */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
    	if (dontSlideNow) return false;
		tX = (int)event.getX() ;
		tXtoMove = tX - tXinner;
		checkXLimits(false);
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			toleranceOver= true;
			tXinner = (int)event.getX();
			xOld = tXtoMove;
		}
		Log.d(LOG,"tX "+tX+ "    tXinner "+tXinner);
		if(toleranceOver){
			movement = new TranslateAnimation(xOld , tXtoMove , 0 , 0);
			movement.setDuration(0);
			movement.setFillAfter(true);
			screen.get(currSlideNumber).startAnimation(movement);
		    if(currSlideNumber<screen.size()){
		    	Log.d(LOG,"+1 anim");
		    	Animation movement2 = new TranslateAnimation(xOld , tXtoMove , 0 , 0);
		    	movement2.setDuration(0);
		    	movement2.setFillAfter(true);
				screen.get(currSlideNumber+1).startAnimation(movement2);
		    }
		    xOld = tXtoMove;
		}
		if (event.getAction() == MotionEvent.ACTION_UP) {
			animateCorrection();
		}
		redrawPlease();
		return true;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    /**
     * ������ �������� ������� ������� ������ �� ������� ����
     */
    private void animateCorrection(){
    	
		int xTarget = 0;
		if (needCross()) xTarget = screen.get(currSlideNumber).x > maxX/2? -maxX : maxX;
		
		int duration = (int)( ( Math.abs((float)xTarget - (float)xOld)/ (float)maxX )/CORR_SPEED );
		if (duration < 0) duration = 0;
		
		correcting = new TranslateAnimation( xOld , xTarget , 0 , 0 );
		Log.d(LOG, "xOld "+xOld+"  tXtoMove "+tXtoMove);
		correcting.setDuration( duration );
		correcting.setFillAfter(true);
		correcting.setInterpolator(interpolator);
		correcting.setAnimationListener(new AnimationListener() {
			public void onAnimationStart(Animation animation) {}
			public void onAnimationRepeat(Animation animation) {}
			public void onAnimationEnd(Animation animation) {
				endTranslate();
			}
		});
		screen.get(currSlideNumber).startAnimation(correcting);
    }
    
    
    
    
    
    
    /**
     * ���������� ������������ - ��������� ��������, ��������������� ���������
     */
    private void endTranslate(){
		checkXLimits(true);
		screen.get(currSlideNumber).layout((int)tXtoMove, 0, (int)(tXtoMove+screen.get(currSlideNumber).w), (int)layH);
        screen.get(currSlideNumber).clearAnimation();
        redrawPlease();
        setState();
        movement = null;
        correcting = null;
    }
    
    
    
    
    
    /**
     * �������� ���������� �� ��������
     */
    public void redrawPlease(){
        this.invalidate();
    }
    
    
    
    
    
    /**
     * ����� �� ����������� �����������
     */
    private boolean needCross(){
    	boolean cross = false;
		if (screen.get(currSlideNumber).x < maxX/2 ){
			if (tXtoMove > dividerXleft) cross = true; }
		else{
			if (tXtoMove < -dividerXright) cross = true; }
		Log.d(LOG,"needCross "+cross );
    	return cross;
    }
    
    
    
    
    
    
    /**
     * �������� �� �������� �� X �� ���������� ������� + ���������� �������
     */
    private void checkXLimits(boolean endPositionCheck){
		Log.i(LOG, "xOld "+xOld+"  tXtoMove "+tXtoMove);
    	
    	if(currSlideNumber == 0){
	    	// ����������� � �� ����� ��������
			if (tX >= maxX) tX = maxX;
			
			// ������� ����� ����������
			if (endPositionCheck) {
				// ���� ��� ����� ��������� �� ������ ��������
				if (state == STATE_NORMAL){
					Log.d(LOG,"state == STATE_NORMAL   tXtoMove"+tXtoMove );
					if (tXtoMove < dividerXleft) tXtoMove=0;
					else tXtoMove = maxX;
					Log.d(LOG,"state == STATE_NORMAL   tXtoMove"+tXtoMove );
				}
				// ������
				else{
					Log.d(LOG,"state == STATE_MENU   tXtoMove"+tXtoMove+"  xOld "+xOld );
					if (tXtoMove < -dividerXright) tXtoMove= 0;
					else tXtoMove = maxX;
					Log.d(LOG,"state == STATE_MENU   tXtoMove"+tXtoMove );
				}
			} 
    	}
    	if (endPositionCheck) {
	    	if (tXstart>0 && tX < tXstart-layW/2){
	    		leftMenu.setVisibility(View.GONE);
	    		screen.get(currSlideNumber).layout(-layW, 0, 0, layH);
	    		currSlideNumber++;
	    		screen.get(currSlideNumber).layout(0, 0, layW, layH);
	    		screen.get(currSlideNumber).setVisibility(View.GONE);
	    		Log.d(LOG,"currSlideNumber "+currSlideNumber );
	    	}
	    	else if (tXstart>0 && tX > tXstart-layW/2){
	    		if(currSlideNumber>0){
		    		screen.get(currSlideNumber).layout(layW, 0, layW*2, layH);
	    			currSlideNumber--;
		    		screen.get(currSlideNumber).layout(0, 0, layW, layH);
		    		screen.get(currSlideNumber).setVisibility(View.VISIBLE);
		    		if (currSlideNumber==0){
		    			leftMenu.setVisibility(View.VISIBLE);
		    		}
		    		Log.d(LOG,"currSlideNumber "+currSlideNumber );
	    		}
	    	}
    	}
    }
    
    
    
    
    /**
     * ������ view ��� ������� �� ������� ���� �� ����� ��������� /
     * Set the view when you click on that layer will not move
     * @param view
     */
    public void setIgnoredSlidingView(View view){
    	view.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				Log.d(LOG, "ignored view touched");
				setCanSlide();
				if (event.getAction() == MotionEvent.ACTION_DOWN || event.getAction() == MotionEvent.ACTION_MOVE){
					setDontSlide();
				}
				return false;
			}
		});
    }
    
}

